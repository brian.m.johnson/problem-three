package secondproblem;

import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ThirdProblem {


	public static void main(String[] args) {
		
		BigInteger largeNumber = new BigInteger("600851475143");
		BigInteger zeroNumber = new BigInteger("0");
		int sizeOfArray = 0;
		
		List<BigInteger> resultsArray = new ArrayList<BigInteger>();
		
		for (BigInteger i = BigInteger.valueOf(2);
			i.compareTo(largeNumber) <= 0;
			i = i.add(BigInteger.ONE)) {
			
			BigInteger remainderNumber = new BigInteger(String.valueOf(i));
			BigInteger result[] = largeNumber.divideAndRemainder(remainderNumber);
			int comparevalue = result[1].compareTo(zeroNumber);
            if (comparevalue == 0) {
            	largeNumber = largeNumber.divide(remainderNumber);
            	resultsArray.add(i);
            	sizeOfArray++;
            	
            }

        }
		
		Collections.sort(resultsArray);
		System.out.println("Largest prime factor of the number 600851475143: " + NumberFormat.getInstance().format(resultsArray.get(sizeOfArray - 1)));
        
	}

}
